import { Component, OnInit } from '@angular/core';
import { RemetentesService } from 'src/service/remetentes.service';
import { FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-remetente-editar',
  templateUrl: './remetente-editar.component.html',
  styleUrls: ['../remetentes.component.css']
})
export class RemetenteEditarComponent implements OnInit {
  _id: String = '';
  remetenteForm: FormGroup;
  nome: String = '';
  telefone: String = '';
  cpf: String = '';
  endereco: String = '';
  email: String = '';
  isLoadingResults = false;
  constructor(private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private remetentesService:RemetentesService) { }

  ngOnInit() {
      this.remetentePorId(this.route.snapshot.params['id']);
      this.remetenteForm = this.formBuilder.group({
      'nome' : [null],
      'telefone' : [null],
      'cpf' : [null],
      'endereco' : [null],
      'email' : [null]
    });
  }

  remetentePorId(id) {
    this.remetentesService.remetentePorId(id).subscribe(remetente => {
      this._id = remetente._id;
      this.remetenteForm.setValue({
        nome: remetente.nome,
        telefone: remetente.telefone,
        cpf: remetente.cpf,
        endereco: remetente.endereco,
        email: remetente.email
      });
    });
  }

  atualizarRemetente(form: NgForm) {
    this.isLoadingResults = true;
    this.remetentesService.atualizarRemetente(this.route.snapshot.params['id'], form)
      .subscribe(res => {
          this.isLoadingResults = false;
          //this.router.navigate(['/detalhe/' + this._id]);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

}