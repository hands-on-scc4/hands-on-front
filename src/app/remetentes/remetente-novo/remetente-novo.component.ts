import { Component, OnInit } from '@angular/core';
import { RemetentesService } from 'src/service/remetentes.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { Remetente } from 'src/model/remetente';

@Component({
  selector: 'app-remetente-novo',
  templateUrl: './remetente-novo.component.html',
  styleUrls: ['../remetentes.component.css']
})
export class RemetenteNovoComponent implements OnInit {

  public remetente:Remetente = new Remetente()  
  constructor(private router: Router, private remetentesService:RemetentesService, private formBuilder: FormBuilder) { }

  ngOnInit() {}

  public inserir() {
    this.remetentesService.inserirRemetente(this.remetente).subscribe(
      response => {
        //console.log(this.remetente)
        alert("Salvo com sucesso!")
      },
      error => {
        alert("Algo deu errado, registro não foi salvo!")
      }
    )
}

}