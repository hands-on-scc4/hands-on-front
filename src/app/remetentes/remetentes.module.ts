import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { RemetentesRoutingModule } from './remetentes.routing.module';
import { RemetentesComponent } from './remetentes.component';
import { RemetenteDetalheComponent } from './remetente-detalhe/remetente-detalhe.component';
import { RemetenteEditarComponent } from './remetente-editar/remetente-editar.component';
import { RemetenteNovoComponent } from './remetente-novo/remetente-novo.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ RemetentesComponent, RemetenteDetalheComponent, RemetenteEditarComponent, RemetenteNovoComponent ],
  imports: [
    CommonModule,
    RemetentesRoutingModule,
    FormsModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule
  ]
})
export class RemetentesModule { }