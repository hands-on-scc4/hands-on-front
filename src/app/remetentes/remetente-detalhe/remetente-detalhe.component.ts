import { Component, OnInit } from '@angular/core';
import { Remetente } from 'src/model/remetente';
import { RemetentesService } from 'src/service/remetentes.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-remetente-detalhe',
  templateUrl: './remetente-detalhe.component.html',
  styleUrls: ['../remetentes.component.css']
})
export class RemetenteDetalheComponent implements OnInit {
  remetente: Remetente = { _id: '', nome: '', telefone: '', cpf: '', endereco: '', email: '' };
  isLoadingResults = true;
  constructor(private router: Router, private route: ActivatedRoute, private remetentesService:RemetentesService) { }

  ngOnInit() {
    this.emetentePorId(this.route.snapshot.params['id']);
  }

  emetentePorId(id) {
    this.remetentesService.remetentePorId(id)
      .subscribe(data => {
        this.remetente = data;
        console.log(this.remetente);
        this.isLoadingResults = false;
      });
  }

}