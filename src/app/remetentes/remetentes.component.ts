import { Component, OnInit } from '@angular/core';
import { Remetente } from 'src/model/remetente';
import { RemetentesService } from 'src/service/remetentes.service';

@Component({
  selector: 'app-remetentes',
  templateUrl: './remetentes.component.html',
  styleUrls: ['./remetentes.component.css']
})
export class RemetentesComponent implements OnInit {
  displayedColumns: string[] = [ 'nome', 'telefone', 'cpf', 'endereco', 'email'];
  dataSource: Remetente[];
  isLoadingResults = true;
  constructor(private _rService: RemetentesService) { }

  ngOnInit() {
    this.listar()
  }

  public listar() {
    this._rService.listarRemetentes()
    .subscribe(res => {
      this.dataSource = res;
      console.log(this.dataSource);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  public excluir(id:string) {
    this._rService.excluirRemetente(id).subscribe(
      r => {
        this.listar()
      }
    )
  }

}
