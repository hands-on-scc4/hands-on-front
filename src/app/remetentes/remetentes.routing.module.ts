import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RemetentesComponent } from './remetentes.component';
import { RemetenteDetalheComponent } from './remetente-detalhe/remetente-detalhe.component';
import { RemetenteEditarComponent } from './remetente-editar/remetente-editar.component';
import { RemetenteNovoComponent } from './remetente-novo/remetente-novo.component';

const routes: Routes = [
  { 
    path: '',
    component:RemetentesComponent
  },
  { 
    path: 'detalhe/:id',
    component:RemetenteDetalheComponent
  },
  { 
    path: 'editar/:id',
    component:RemetenteEditarComponent
  },
  { 
    path: 'novo',
    component:RemetenteNovoComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class RemetentesRoutingModule { }