import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  items: MenuItem[];

  ngOnInit() {
    this.items = [
      {label: 'Início', icon: 'fa fa-fw fa-bar-home', routerLink:'home'},
      {label: 'Remetentes', icon: 'fa fa-fw fa-calendar', routerLink:'remetentes'},
      {label: 'Destinatários', icon: 'fa fa-fw fa-book'},
      {label: 'Mensagem', icon: 'fa fa-fw fa-support'},
    ];
  }

}