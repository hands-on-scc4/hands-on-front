export class Remetente {

    _id: string;
    public nome:string;
    public telefone:string;
    public cpf:string;
    public endereco:string;
    public email:string;
    
  }