import { TestBed } from '@angular/core/testing';

import { RemetentesService } from './remetentes.service';

describe('RemetentesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemetentesService = TestBed.get(RemetentesService);
    expect(service).toBeTruthy();
  });
});
