import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Remetente } from 'src/model/remetente';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = '/api/remetentes';

@Injectable({
  providedIn: 'root'
})
export class RemetentesService {

  constructor(private http: HttpClient) { }

  listarRemetentes():Observable<Remetente[]> {
    return this.http.get<Remetente[]>(apiUrl)
    .pipe(
      tap(remetentes => console.log('lista de remetentes carregada!')),
      catchError(this.handleError('listarRemetentes', []))
    );
  }

  remetentePorId(id: number): Observable<Remetente> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Remetente>(url).pipe(
      tap(_ => console.log(`Leu o remetente! id=${id}`)),
      catchError(this.handleError<Remetente>(`remetentePorId id=${id}`))
    )
  }

  inserirRemetente(remetente): Observable<Remetente> {
    return this.http.post<Remetente>(apiUrl, remetente, httpOptions).pipe(
      tap((remetente: Remetente) => console.log(`adicionou o remetente com w/ id=${remetente._id}`)),
      catchError(this.handleError<Remetente>('inserirRemetente'))
    );
  }

  atualizarRemetente(id, remetente): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, remetente, httpOptions).pipe(
      tap(_ => console.log(`atualiza o remetente com id=${id}`)),
      catchError(this.handleError<any>('atualizaRemetente'))
    );
  }

  excluirRemetente (id): Observable<Remetente> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Remetente>(url, httpOptions).pipe(
      tap(_ => console.log(`remove o remetente com id=${id}`)),
      catchError(this.handleError<Remetente>('excluirRemetente'))
    );
  }

  //--------
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }

}